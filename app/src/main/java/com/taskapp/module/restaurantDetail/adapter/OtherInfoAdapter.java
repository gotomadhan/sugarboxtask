package com.taskapp.module.restaurantDetail.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.taskapp.R;

import java.util.ArrayList;

public class OtherInfoAdapter extends RecyclerView.Adapter<OtherInfoAdapter.ViewHolder> {

    private ArrayList<String> stringArrayList;

    public OtherInfoAdapter(ArrayList<String> stringArrayList) {
        this.stringArrayList = stringArrayList;
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.other_info_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.infoTxt.setText(stringArrayList.get(position));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView infoTxt;

        ViewHolder(View v) {
            super(v);
            infoTxt = v.findViewById(R.id.info_txt);
        }
    }

}