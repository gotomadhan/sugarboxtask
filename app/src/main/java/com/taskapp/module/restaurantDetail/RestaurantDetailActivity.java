package com.taskapp.module.restaurantDetail;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.taskapp.R;
import com.taskapp.common.ToastMessage;
import com.taskapp.common.Validation;
import com.taskapp.module.restaurantDetail.adapter.OtherInfoAdapter;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;

public class RestaurantDetailActivity extends AppCompatActivity {

    private AppCompatImageView restaurantImage;
    private RatingBar ratingBar;
    private TextView rating, review, restaurantName, casualDining, shortAddress, openingHrs, costForTwo, address, cusinies, avgCost;
    private RestaurantListResponseModel.Restaurant.InnerRestaurant innerRestaurant;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_detail);
        innerRestaurant = (RestaurantListResponseModel.Restaurant.InnerRestaurant) getIntent().getSerializableExtra(getString(R.string.restaurant_model));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            if (innerRestaurant != null) {
                actionBar.setTitle(innerRestaurant.getName());
            }
        }

        recyclerView = findViewById(R.id.other_info_list);
        restaurantImage = findViewById(R.id.image);
        ratingBar = findViewById(R.id.rating_bar);
        rating = findViewById(R.id.rating);
        review = findViewById(R.id.review_count);
        restaurantName = findViewById(R.id.name);
        casualDining = findViewById(R.id.casual_dining);
        shortAddress = findViewById(R.id.short_address);
        openingHrs = findViewById(R.id.opening_hrs);
        costForTwo = findViewById(R.id.cost_for_two);
        address = findViewById(R.id.address);
        cusinies = findViewById(R.id.cuisines);
        avgCost = findViewById(R.id.avg_cost);

        findViewById(R.id.copy_address).setOnClickListener(view -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("label", address.getText().toString());
            clipboard.setPrimaryClip(clip);
            ToastMessage.toast("Location Copied");
        });

        updateText();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    private void updateText() {
        if (innerRestaurant != null) {

            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.ic_no_image)).load(innerRestaurant.getThumb()).into(restaurantImage);

            if (innerRestaurant.getUser_rating() != null) {
                rating.setText(innerRestaurant.getUser_rating().getAggregate_rating());
                ratingBar.setRating(Validation.convertStringToFloat(innerRestaurant.getUser_rating().getAggregate_rating()));
            }

            review.setText(String.format("%s REVIEWS", innerRestaurant.getAll_reviews_count()));
            restaurantName.setText(innerRestaurant.getName());
            casualDining.setText(String.format("Casual Dining - %s", innerRestaurant.getCuisines()));

            if (innerRestaurant.getLocation() != null) {
                shortAddress.setText(String.format("%s, %s", innerRestaurant.getLocation().getLocality(), innerRestaurant.getLocation().getCity()));
                address.setText(innerRestaurant.getLocation().getAddress());
            }

            openingHrs.setText(innerRestaurant.getTimings());
            costForTwo.setText(String.format("Cost for two - %s%s (approx.)", innerRestaurant.getCurrency(), innerRestaurant.getAverage_cost_for_two()));
            cusinies.setText(innerRestaurant.getCuisines());
            avgCost.setText(String.format("%s%s for two people (approx.)", innerRestaurant.getCurrency(), innerRestaurant.getAverage_cost_for_two()));

            if (innerRestaurant.getHighlights() != null && innerRestaurant.getHighlights().size() > 0) {
                OtherInfoAdapter otherInfoAdapter = new OtherInfoAdapter(innerRestaurant.getHighlights());
                LinearLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
                recyclerView.setLayoutManager(gridLayoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(otherInfoAdapter);
            }

        }
    }


}
