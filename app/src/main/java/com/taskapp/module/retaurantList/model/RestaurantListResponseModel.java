package com.taskapp.module.retaurantList.model;

import java.io.Serializable;
import java.util.ArrayList;

public class RestaurantListResponseModel {

    private ArrayList<Restaurant> restaurants;

    public ArrayList<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(ArrayList<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    public static class Restaurant {

        private InnerRestaurant restaurant;

        public InnerRestaurant getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(InnerRestaurant restaurant) {
            this.restaurant = restaurant;
        }

        public static class InnerRestaurant implements Serializable {

            private RestaurantDetail R;

            private String name;

            private Location location;

            private String average_cost_for_two;

            private String timings;

            private ArrayList<String> highlights;

            private String price_range;

            private String currency;

            private String switch_to_order_menu;

            private String cuisines;

            private String thumb;

            private String opentable_support;

            private String is_zomato_book_res;

            private String book_again_url;

            private String is_book_form_web_view;

            private String book_form_web_view_url;

            private String mezzo_provider;

            private UserRating user_rating;

            private String include_bogo_offers;

            private String has_online_delivery;

            private String has_table_booking;

            private String menu_url;

            private int is_delivering_now;

            private String deeplink;

            private String order_deeplink;

            private String store_type;

            private String featured_image;

            private String medio_provider;

            private String order_url;

            private String phone_numbers;

            private String photo_count;

            private String events_url;

            private String all_reviews_count;

            private String is_table_reservation_supported;

            private String photos_url;

            public ArrayList<String> getHighlights() {
                return highlights;
            }

            public void setHighlights(ArrayList<String> highlights) {
                this.highlights = highlights;
            }

            public RestaurantDetail getRestaurant() {
                return R;
            }

            public void setRestaurant(RestaurantDetail restaurant) {
                this.R = restaurant;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Location getLocation() {
                return location;
            }

            public void setLocation(Location location) {
                this.location = location;
            }

            public String getAverage_cost_for_two() {
                return average_cost_for_two;
            }

            public void setAverage_cost_for_two(String average_cost_for_two) {
                this.average_cost_for_two = average_cost_for_two;
            }

            public String getTimings() {
                return timings;
            }

            public void setTimings(String timings) {
                this.timings = timings;
            }

            public String getPrice_range() {
                return price_range;
            }

            public void setPrice_range(String price_range) {
                this.price_range = price_range;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getSwitch_to_order_menu() {
                return switch_to_order_menu;
            }

            public void setSwitch_to_order_menu(String switch_to_order_menu) {
                this.switch_to_order_menu = switch_to_order_menu;
            }

            public String getCuisines() {
                return cuisines;
            }

            public void setCuisines(String cuisines) {
                this.cuisines = cuisines;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }

            public String getOpentable_support() {
                return opentable_support;
            }

            public void setOpentable_support(String opentable_support) {
                this.opentable_support = opentable_support;
            }

            public String getIs_zomato_book_res() {
                return is_zomato_book_res;
            }

            public void setIs_zomato_book_res(String is_zomato_book_res) {
                this.is_zomato_book_res = is_zomato_book_res;
            }

            public String getBook_again_url() {
                return book_again_url;
            }

            public void setBook_again_url(String book_again_url) {
                this.book_again_url = book_again_url;
            }

            public String getIs_book_form_web_view() {
                return is_book_form_web_view;
            }

            public void setIs_book_form_web_view(String is_book_form_web_view) {
                this.is_book_form_web_view = is_book_form_web_view;
            }

            public String getBook_form_web_view_url() {
                return book_form_web_view_url;
            }

            public void setBook_form_web_view_url(String book_form_web_view_url) {
                this.book_form_web_view_url = book_form_web_view_url;
            }

            public String getMezzo_provider() {
                return mezzo_provider;
            }

            public void setMezzo_provider(String mezzo_provider) {
                this.mezzo_provider = mezzo_provider;
            }

            public UserRating getUser_rating() {
                return user_rating;
            }

            public void setUser_rating(UserRating user_rating) {
                this.user_rating = user_rating;
            }

            public String getInclude_bogo_offers() {
                return include_bogo_offers;
            }

            public void setInclude_bogo_offers(String include_bogo_offers) {
                this.include_bogo_offers = include_bogo_offers;
            }

            public String getHas_online_delivery() {
                return has_online_delivery;
            }

            public void setHas_online_delivery(String has_online_delivery) {
                this.has_online_delivery = has_online_delivery;
            }

            public String getHas_table_booking() {
                return has_table_booking;
            }

            public void setHas_table_booking(String has_table_booking) {
                this.has_table_booking = has_table_booking;
            }

            public String getMenu_url() {
                return menu_url;
            }

            public void setMenu_url(String menu_url) {
                this.menu_url = menu_url;
            }

            public int getIs_delivering_now() {
                return is_delivering_now;
            }

            public void setIs_delivering_now(int is_delivering_now) {
                this.is_delivering_now = is_delivering_now;
            }

            public String getDeeplink() {
                return deeplink;
            }

            public void setDeeplink(String deeplink) {
                this.deeplink = deeplink;
            }

            public String getOrder_deeplink() {
                return order_deeplink;
            }

            public void setOrder_deeplink(String order_deeplink) {
                this.order_deeplink = order_deeplink;
            }

            public String getStore_type() {
                return store_type;
            }

            public void setStore_type(String store_type) {
                this.store_type = store_type;
            }

            public String getFeatured_image() {
                return featured_image;
            }

            public void setFeatured_image(String featured_image) {
                this.featured_image = featured_image;
            }

            public String getMedio_provider() {
                return medio_provider;
            }

            public void setMedio_provider(String medio_provider) {
                this.medio_provider = medio_provider;
            }

            public String getOrder_url() {
                return order_url;
            }

            public void setOrder_url(String order_url) {
                this.order_url = order_url;
            }

            public String getPhone_numbers() {
                return phone_numbers;
            }

            public void setPhone_numbers(String phone_numbers) {
                this.phone_numbers = phone_numbers;
            }

            public String getPhoto_count() {
                return photo_count;
            }

            public void setPhoto_count(String photo_count) {
                this.photo_count = photo_count;
            }

            public String getEvents_url() {
                return events_url;
            }

            public void setEvents_url(String events_url) {
                this.events_url = events_url;
            }

            public String getAll_reviews_count() {
                return all_reviews_count;
            }

            public void setAll_reviews_count(String all_reviews_count) {
                this.all_reviews_count = all_reviews_count;
            }

            public String getIs_table_reservation_supported() {
                return is_table_reservation_supported;
            }

            public void setIs_table_reservation_supported(String is_table_reservation_supported) {
                this.is_table_reservation_supported = is_table_reservation_supported;
            }

            public String getPhotos_url() {
                return photos_url;
            }

            public void setPhotos_url(String photos_url) {
                this.photos_url = photos_url;
            }

            public static class UserRating implements Serializable {
                private String aggregate_rating;

                private String rating_color;

                private String rating_text;

                private String votes;

                public String getAggregate_rating() {
                    return aggregate_rating;
                }

                public void setAggregate_rating(String aggregate_rating) {
                    this.aggregate_rating = aggregate_rating;
                }

                public String getRating_color() {
                    return rating_color;
                }

                public void setRating_color(String rating_color) {
                    this.rating_color = rating_color;
                }

                public String getRating_text() {
                    return rating_text;
                }

                public void setRating_text(String rating_text) {
                    this.rating_text = rating_text;
                }

                public String getVotes() {
                    return votes;
                }

                public void setVotes(String votes) {
                    this.votes = votes;
                }
            }

            public static class Location implements Serializable {
                private String zipcode;

                private String address;

                private String city;

                private String locality_verbose;

                private String latitude;

                private String locality;

                private String country_id;

                private String city_id;

                private String longitude;

                public String getZipcode() {
                    return zipcode;
                }

                public void setZipcode(String zipcode) {
                    this.zipcode = zipcode;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getCity() {
                    return city;
                }

                public void setCity(String city) {
                    this.city = city;
                }

                public String getLocality_verbose() {
                    return locality_verbose;
                }

                public void setLocality_verbose(String locality_verbose) {
                    this.locality_verbose = locality_verbose;
                }

                public String getLatitude() {
                    return latitude;
                }

                public void setLatitude(String latitude) {
                    this.latitude = latitude;
                }

                public String getLocality() {
                    return locality;
                }

                public void setLocality(String locality) {
                    this.locality = locality;
                }

                public String getCountry_id() {
                    return country_id;
                }

                public void setCountry_id(String country_id) {
                    this.country_id = country_id;
                }

                public String getCity_id() {
                    return city_id;
                }

                public void setCity_id(String city_id) {
                    this.city_id = city_id;
                }

                public String getLongitude() {
                    return longitude;
                }

                public void setLongitude(String longitude) {
                    this.longitude = longitude;
                }
            }

            public static class RestaurantDetail implements Serializable {
                private HasMenuStatus has_menu_status;

                private String res_id;

                private String is_grocery_store;

                public HasMenuStatus getHas_menu_status() {
                    return has_menu_status;
                }

                public void setHas_menu_status(HasMenuStatus has_menu_status) {
                    this.has_menu_status = has_menu_status;
                }

                public String getRes_id() {
                    return res_id;
                }

                public void setRes_id(String res_id) {
                    this.res_id = res_id;
                }

                public String getIs_grocery_store() {
                    return is_grocery_store;
                }

                public void setIs_grocery_store(String is_grocery_store) {
                    this.is_grocery_store = is_grocery_store;
                }

                public static class HasMenuStatus implements Serializable {
                    private String delivery;

                    private String takeaway;

                    public String getDelivery() {
                        return delivery;
                    }

                    public void setDelivery(String delivery) {
                        this.delivery = delivery;
                    }

                    public String getTakeaway() {
                        return takeaway;
                    }

                    public void setTakeaway(String takeaway) {
                        this.takeaway = takeaway;
                    }

                }

            }
        }


    }

}
