package com.taskapp.module.retaurantList.view;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.taskapp.R;
import com.taskapp.common.BaseActivity;
import com.taskapp.common.ToastMessage;
import com.taskapp.module.restaurantDetail.RestaurantDetailActivity;
import com.taskapp.module.retaurantList.adapter.IRecyclerViewClick;
import com.taskapp.module.retaurantList.adapter.RestaurantListAdapter;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;
import com.taskapp.module.retaurantList.presenter.IRestaurantPresenter;
import com.taskapp.module.retaurantList.presenter.RestaurantListPresenter;
import com.taskapp.module.searchRestaurant.SearchActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RestaurantListActivity extends BaseActivity implements IRestaurantListView, IRecyclerViewClick {

    private RestaurantListAdapter restaurantListAdapter;
    private ArrayList<RestaurantListResponseModel.Restaurant> resultsArrayList = new ArrayList<>();
    private IRestaurantPresenter.presenter iRestaurantPresenter;
    private int pageCount = 1, totalItemCount = 0, recordCount = 20;
    private boolean isLoading;

    private FusedLocationProviderClient mFusedLocationClient;
    private final int REQUEST_CHECK_SETTINGS = 100;
    private final int REQUEST_LOCATION = 1;
    private LocationRequest locationRequest;
    private String lat = "0", lang = "0";

    private TextView areaName, noRecordText;
    private ProgressBar progressBar;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);
        RecyclerView recyclerView = findViewById(R.id.restaurant_list);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("NearBy Restaurant");
        }

        restaurantListAdapter = new RestaurantListAdapter(resultsArrayList, this);
        LinearLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(restaurantListAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = gridLayoutManager.getChildCount();
                int firstVisibleItemPosition = gridLayoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && totalItemCount >= recordCount && !isLoading) {
                    callApi();
                }
            }
        });

        noRecordText = findViewById(R.id.no_record);
        areaName = findViewById(R.id.area_name);
        progressBar = findViewById(R.id.progress_bar);

        iRestaurantPresenter = new RestaurantListPresenter(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        getCurrentLocationLatLng();

        findViewById(R.id.location_layout).setOnClickListener(view -> gotoSearchActivity());

        bottomNavigationView = findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                String title = item.getTitle().toString();
                if (title.equals(getString(R.string.search))) {
                    gotoSearchActivity();
                    new Handler().postDelayed(() -> bottomNavigationView.getMenu().getItem(0).setChecked(true), 500);
                }
                return true;
            }
        });
    }

    private void gotoSearchActivity() {
        startActivity(new Intent(RestaurantListActivity.this, SearchActivity.class)
                .putExtra(getString(R.string.lat), lat)
                .putExtra(getString(R.string.lang), lang));
    }

    private void callApi() {
        progressBar.setVisibility(pageCount > 1 ? View.VISIBLE : View.GONE);
        iRestaurantPresenter.getList(lat, lang, totalItemCount, "", false);
        isLoading = true;
    }

    @Override
    public void clickPosition(int position) {
        startActivity(new Intent(this, RestaurantDetailActivity.class)
                .putExtra(getString(R.string.restaurant_model), resultsArrayList.get(position).getRestaurant()));
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void restaurantListResponse(ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList) {
        resultsArrayList.addAll(restaurantArrayList);
        totalItemCount += recordCount;
        pageCount++;
        isLoading = false;
        restaurantListAdapter.notifyData(resultsArrayList);
        noRecordText.setVisibility(View.GONE);
    }

    @Override
    public void noRecord() {
        if (resultsArrayList.size() == 0) {
            noRecordText.setVisibility(View.VISIBLE);
        }
    }


    public void getCurrentLocationLatLng() {
        showProgress();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            hideProgress();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            checkGPSEnable();
        }
    }

    private void checkGPSEnable() {
        showProgress();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        result.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.
                getLocation();
            } catch (ApiException exception) {
                hideProgress();
                switch (exception.getStatusCode()) {

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                    RestaurantListActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            ToastMessage.toast("Can't enable location");
                            // Ignore the error.
                        } catch (ClassCastException e) {
                            ToastMessage.toast("Can't enable location");
                            // Ignore, should be an impossible error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        hideProgress();
                        ToastMessage.toast("Can't enable location");
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    public void getLocation() {
        showProgress();
        if (mFusedLocationClient == null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            hideProgress();
            return;
        }

        final LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                checkLocation(locationResult.getLastLocation());
                LocationServices.getFusedLocationProviderClient(RestaurantListActivity.this).removeLocationUpdates(this);
            }
        };

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    if (location == null) {
                        if (ActivityCompat.checkSelfPermission(RestaurantListActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RestaurantListActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        LocationServices.getFusedLocationProviderClient(RestaurantListActivity.this).requestLocationUpdates(locationRequest, mLocationCallback, null);
                    } else {
                        checkLocation(location);
                    }

                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
//                        ToastMessage.toast("Can't get location try again");
                    if (ActivityCompat.checkSelfPermission(RestaurantListActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RestaurantListActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    LocationServices.getFusedLocationProviderClient(RestaurantListActivity.this).requestLocationUpdates(locationRequest, mLocationCallback, null);
                });

    }


    private void checkLocation(Location location) {
        if (location != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                hideProgress();
                return;
            }
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                lat = String.valueOf(location.getLatitude());
                lang = String.valueOf(location.getLongitude());
                if (addresses.size() > 0) {
                    areaName.setText(addresses.get(0).getSubLocality());
                }
                callApi();
            } catch (Exception e) {
                hideProgress();
                ToastMessage.toast("Can't get location try again");
                e.printStackTrace();
            }
        } else {
            hideProgress();
            ToastMessage.toast("Can't get location try again");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            checkGPSEnable();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                showProgress();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation();
                    }
                }, 1000);

            } else {
                ToastMessage.toast("GPS is not enabled");
            }
        }
    }


}
