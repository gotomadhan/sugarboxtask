package com.taskapp.module.retaurantList.presenter;

import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;

public interface IRestaurantPresenter {
    interface presenter {
        void getList(String lat, String lng, int itemPosition, String restaurantName, boolean isFromSearch);
    }

    interface model {
        void getRestaurant(String lat, String lng, int itemPosition, String restaurantName, boolean isFromSearch, IRestaurantPresenter.model.OnFinishedListener onFinishedListener);

        interface OnFinishedListener {
            void onFinished(RestaurantListResponseModel restaurantListResponseModel);

            void onFailure(Throwable t);
        }
    }

}
