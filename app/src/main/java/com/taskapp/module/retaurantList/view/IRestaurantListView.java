package com.taskapp.module.retaurantList.view;


import com.taskapp.common.IBaseView;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;

import java.util.ArrayList;

public interface IRestaurantListView extends IBaseView {
    void restaurantListResponse(ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList);

    void noRecord();
}
