package com.taskapp.module.retaurantList.presenter;

import com.taskapp.module.retaurantList.model.RestaurantListRepository;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;
import com.taskapp.module.retaurantList.view.IRestaurantListView;

public class RestaurantListPresenter implements IRestaurantPresenter.presenter, IRestaurantPresenter.model.OnFinishedListener {

    private IRestaurantListView iRestaurantListView;
    private IRestaurantPresenter.model model;

    public RestaurantListPresenter(IRestaurantListView iRestaurantListView) {
        this.iRestaurantListView = iRestaurantListView;
        model = new RestaurantListRepository();
    }

    @Override
    public void getList(String lat, String lng, int itemPosition, String restaurantName, boolean isFromSearch) {
        if (itemPosition == 0 && !isFromSearch) {
            iRestaurantListView.showProgress();
        }
        model.getRestaurant(lat, lng, itemPosition, restaurantName, isFromSearch, this);
    }

    @Override
    public void onFinished(RestaurantListResponseModel response) {
        iRestaurantListView.hideProgress();
        if (response.getRestaurants() != null && response.getRestaurants().size() > 0) {
            iRestaurantListView.restaurantListResponse(response.getRestaurants());
        }else {
            iRestaurantListView.noRecord();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        iRestaurantListView.noRecord();
        iRestaurantListView.hideProgress();
    }
}
