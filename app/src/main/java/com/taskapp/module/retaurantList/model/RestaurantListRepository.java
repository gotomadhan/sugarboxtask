package com.taskapp.module.retaurantList.model;

import androidx.annotation.NonNull;

import com.taskapp.common.NetworkUtils;
import com.taskapp.module.retaurantList.presenter.IRestaurantPresenter;
import com.taskapp.webClient.ApiClient;
import com.taskapp.webClientHandler.GetRestaurantListAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantListRepository implements IRestaurantPresenter.model {

    @Override
    public void getRestaurant(String lat, String lng, int itemPosition, String restaurantName, boolean isFromSearch, IRestaurantPresenter.model.OnFinishedListener onFinishedListener) {
        if (NetworkUtils.checkNetworkConnection()) {
            GetRestaurantListAPI getRestaurantListAPI = ApiClient.getClient().create(GetRestaurantListAPI.class);
            Call<RestaurantListResponseModel> call = getRestaurantListAPI.getRestaurantList(lat, lng, itemPosition, 20, restaurantName);
            call.enqueue(new Callback<RestaurantListResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<RestaurantListResponseModel> call, @NonNull Response<RestaurantListResponseModel> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        onFinishedListener.onFinished(response.body());
                    } else {
                        onFinishedListener.onFailure(new Throwable());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RestaurantListResponseModel> call, @NonNull Throwable t) {
                    onFinishedListener.onFailure(t);
                }
            });
        }
    }
}


