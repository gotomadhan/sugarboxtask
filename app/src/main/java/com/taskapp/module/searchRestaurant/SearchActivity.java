package com.taskapp.module.searchRestaurant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taskapp.R;
import com.taskapp.common.BaseActivity;
import com.taskapp.module.restaurantDetail.RestaurantDetailActivity;
import com.taskapp.module.retaurantList.adapter.IRecyclerViewClick;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;
import com.taskapp.module.retaurantList.presenter.IRestaurantPresenter;
import com.taskapp.module.retaurantList.presenter.RestaurantListPresenter;
import com.taskapp.module.retaurantList.view.IRestaurantListView;
import com.taskapp.module.searchRestaurant.adapter.RestaurantSearchAdapter;

import java.util.ArrayList;

public class SearchActivity extends BaseActivity implements IRestaurantListView, IRecyclerViewClick {

    private RestaurantSearchAdapter restaurantSearchAdapter;
    private ArrayList<RestaurantListResponseModel.Restaurant> resultsArrayList = new ArrayList<>();
    private IRestaurantPresenter.presenter iRestaurantPresenter;
    private String lat = "0", lang = "0";
    private EditText searchText;
    private TextView noRecordText;
    private Handler handler;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        iRestaurantPresenter = new RestaurantListPresenter(this);
        lat = getIntent().getStringExtra(getString(R.string.lat));
        lang = getIntent().getStringExtra(getString(R.string.lang));

        noRecordText = findViewById(R.id.no_record);
        searchText = findViewById(R.id.search_txt);
        searchText.requestFocus();

        recyclerView = findViewById(R.id.restaurant_list);
        restaurantSearchAdapter = new RestaurantSearchAdapter(resultsArrayList, this);
        LinearLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(restaurantSearchAdapter);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (handler == null) {
                    handler = new Handler();
                } else {
                    handler.removeCallbacks(runnable);
                    int stringLength = editable.toString().length();
                    if (editable.toString().isEmpty() || (resultsArrayList.size() == 0 && stringLength <= 1)) {
                        noRecord();
                    } else if (stringLength > 1) {
                        handler.postDelayed(runnable, 300);
                    }
                }
            }
        });

        findViewById(R.id.back_icon).setOnClickListener(view -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
            finish();
        });

    }

    private Runnable runnable = this::callApi;

    private void callApi() {
        iRestaurantPresenter.getList(lat, lang, 0, searchText.getText().toString(), true);
    }

    @Override
    public void clickPosition(int position) {
        startActivity(new Intent(this, RestaurantDetailActivity.class)
                .putExtra(getString(R.string.restaurant_model), resultsArrayList.get(position).getRestaurant()));
    }

    @Override
    public void restaurantListResponse(ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList) {
        resultsArrayList = restaurantArrayList;
        recyclerView.setVisibility(View.VISIBLE);
        noRecordText.setVisibility(View.GONE);
        restaurantSearchAdapter.notifyData(resultsArrayList);
    }

    @Override
    public void noRecord() {
        resultsArrayList.clear();
        recyclerView.setVisibility(View.GONE);
        noRecordText.setVisibility(View.VISIBLE);
    }

}
