package com.taskapp.module.searchRestaurant.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.taskapp.R;
import com.taskapp.module.retaurantList.adapter.IRecyclerViewClick;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;

import java.util.ArrayList;

public class RestaurantSearchAdapter extends RecyclerView.Adapter<RestaurantSearchAdapter.ViewHolder> {

    private ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList;
    private Context context;

    public RestaurantSearchAdapter(ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList, Context context) {
        this.restaurantArrayList = restaurantArrayList;
        this.context = context;
    }

    public void notifyData(ArrayList<RestaurantListResponseModel.Restaurant> restaurantArrayList) {
        this.restaurantArrayList = restaurantArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return restaurantArrayList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_search_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        RestaurantListResponseModel.Restaurant results = restaurantArrayList.get(holder.getAdapterPosition());
        holder.restaurantName.setText(results.getRestaurant().getName());
        if (results.getRestaurant().getLocation() != null) {
            holder.restaurantLocation.setText(results.getRestaurant().getLocation().getLocality());
        } else {
            holder.restaurantLocation.setText("-");
        }
        if (results.getRestaurant().getRestaurant() != null && results.getRestaurant().getIs_delivering_now() == 1) {
            holder.deliveryNotAvailable.setVisibility(View.VISIBLE);
        } else {
            holder.deliveryNotAvailable.setVisibility(View.GONE);
        }

        Glide.with(context).setDefaultRequestOptions(new RequestOptions().error(R.drawable.ic_no_image)).load(results.getRestaurant().getThumb()).into(holder.restaurantImage);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView restaurantName, restaurantLocation, deliveryNotAvailable;
        private ImageView restaurantImage;

        ViewHolder(View v) {
            super(v);
            restaurantName = v.findViewById(R.id.restaurant_name);
            restaurantLocation = v.findViewById(R.id.restaurant_location);
            deliveryNotAvailable = v.findViewById(R.id.delivery_not_available);
            restaurantImage = v.findViewById(R.id.image);
            v.findViewById(R.id.parent_layout).setOnClickListener(view -> ((IRecyclerViewClick) context).clickPosition(getAdapterPosition()));
        }
    }

}