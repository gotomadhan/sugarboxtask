package com.taskapp.webClientHandler;


import com.taskapp.common.Constants;
import com.taskapp.module.retaurantList.model.RestaurantListResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface GetRestaurantListAPI {

    @Headers("user-key: 4feaa2167c4dc6beadf629319423bd4b")
    @GET(Constants.SEARCH_RESTAURANT)
    Call<RestaurantListResponseModel> getRestaurantList(@Query("lat") String lat, @Query("lon") String lon, @Query("start") int start,
                                                        @Query("count") int count, @Query("q") String search);

}
