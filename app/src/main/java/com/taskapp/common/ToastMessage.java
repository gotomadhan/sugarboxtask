package com.taskapp.common;

import android.widget.Toast;

import com.taskapp.SugarBoxTask;

public class ToastMessage {
    public static void toast(String s){
        Toast.makeText(SugarBoxTask.getApplicationInstance(), s, Toast.LENGTH_SHORT).show();
    }
}
