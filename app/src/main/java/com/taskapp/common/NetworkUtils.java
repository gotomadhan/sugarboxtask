package com.taskapp.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.taskapp.SugarBoxTask;


public class NetworkUtils {

    public static boolean checkNetworkConnection() {
        ConnectivityManager ConnectionManager = (ConnectivityManager) SugarBoxTask.getApplicationInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            ToastMessage.toast("No internet connection");
            return false;
        }
    }

}
