package com.taskapp.common;

public interface IBaseView {
    void showProgress();
    void hideProgress();
}
